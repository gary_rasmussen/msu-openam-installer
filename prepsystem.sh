#!/bin/bash

INSTALLUSER="root"     # you need to run the installer as this user

USER=`whoami`
if [[ "$USER" != "$INSTALLUSER" ]]; then
    echo "ERROR:  You are executing this script as: $USER.  You must execute this script as: $INSTALLUSER."
    echo
    exit
fi

groupadd frock
useradd -d /home/frock -m  -g frock -c "ForgeRock User" -s /bin/bash frock

cd /tmp
cp -R /tmp/Installer /home/frock/
mkdir -p /opt/forgerock
chown -R frock:frock /home/frock
chown -R frock:frock /opt/forgerock
su - frock


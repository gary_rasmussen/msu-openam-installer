#!/bin/bash

# $SHOWPASS determines whether passwords should be obscured when printed to the console.
# A value of 0 obscures passwords. A value of 1 shows passwords.
SHOWPASS=1

# $DJDIR is the path to the OpenDJ instance that will be acted upon by this script.
#
# Example: DJDIR=/opt/forgerock/opendj-tokens
DJDIR=/opt/forgerock/opendj-cts

# $initHost is the host that you would like to utilize to populate the entries of the other
# OpenDJ instances.
#
# Use the FQDN only. NEVER use an IP address.
#
# Example: initHost=initHostname.ec2.internal
initHost=initHostname.ec2.internal


# @newHosts is a space delimited array of FQDNs of all the other OpenDJ instances.
# Use the FQDN only. NEVER use an IP address.
#
# Example: newHosts=( newhost1.ec2.internal newhost2.ec2.internal newhost3.ec2.internal )
newHosts=( newhost1.ec2.internal newhost2.ec2.internal newhost3.ec2.internal )


# @noReplicationServerHosts is a space delimited array of FQDNs of any of the other OpenDJ instances which
# should not have a replication server configured on it. The FQDN must exist in @newHosts above.
# For example, if newhost2 listed above is not supposed to have a replication server, use:
# noReplicationServerHosts=( newhost2.ec2.internal )
#
# If all of the newHosts should have a replication server configured, use the following:
# noReplicationServerHosts=( nohosts )
#
# Example: noReplicationServerHosts=( newhost2.ec2.internal )
noReplicationServerHosts=( nohosts )


# $adminPort is the number of the admin port used for the initHost. 
# $adminPort2 is the number of the admin port used for the other OpenDJ instances.
# If the ports are the same, you can set adminPort2 to have the same value as adminPort.
#
# Example: adminPort=4444
# adminPort2=adminPort
adminPort=4444
adminPort2=adminPort


# $repPort is the number of the replication port used for the initHost.
# $repPort2 is the number of the replication port used for the other OpenDJ instances.
# If the ports are the same, you can set repPort2 to have the same value as repPort.
#
# Example: repPort=4989
# repPort2=$repPort
repPort=4989
repPort2=$repPort


# @baseDNs is a space delimited array of the baseDNs that correspond with backends that are 
# to be configured for replication. 
#
# You can get this list from: 
# ldapsearch -T -s one -b "cn=Backends,cn=config" '(&(objectclass=ds-cfg-local-db-backend)(ds-cfg-enabled=TRUE))' ds-cfg-base-dn
#
# Example: baseDNs=( ou=tokens,dc=openam,dc=forgerock,dc=org )
baseDNs=( ou=tokens,dc=openam,dc=forgerock,dc=org )


# $groupId is the integer assigned to each Data Center for the replication environment.
# This is an advanced feature. Do not use group IDs if you are unsure if you should use them.
# If you do not want to use group IDs, set groupID to 1. Otherwise use an integer greater than 1.
groupId=1

# $purgeDelay is the relative time value used by the OpenDJ server to determine how long
# it should retain info in the change log. The default value is "3d", 3 days. Typically
# a purgeDelay of "1d", 1 day, is configured for CTS or Tokens.
#purgeDelay="3d"
purgeDelay="1d"


# $bindPass is the Directory Manager password. You can insert it into this script or find some other
# way to pass it to this script.
#
# Example: bindPass='DirectoryManagerPassword'
bindPass='b8.HzU5eSy@!c'


# $adminPass is the Administrative password. You can insert it into this script or find some other
# way to pass it to this script.
#
# Example: adminPass='AdminPassword'
adminPass='b8.HzU5eSy@!c'



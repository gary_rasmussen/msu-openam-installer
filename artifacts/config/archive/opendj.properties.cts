# OpenDJ Specific Variables

COMPANY="Company Name"                         # Company Name (used for messages in scripts)
INSTALLUSER="frock"                      # Unix OS User OpenDJ Will Run As

# Folder Locations Used in Installation Process
ART="$SCRIPTDIR/artifacts"               # Folder containing customization files
ODJSOFT="$ART/binaries/DS-5.5.0.zip" # Source of the OpenDJ software
ODJLDAPSPATCH="$ART/binaries/DS-5.5.0-201706.zip" # Patch for OpenDJ

INSTANCEDIRNAME="opendj-cts"          # Name of directory which will be created under TMPDIR and DJPARENTDIR

TMPDIR="/tmp"                            # Temporary (Working) Folder
DJSRC="$TMPDIR/opendj"                   # Folder where the extracted OpenDJ is placed

DJPARENTDIR="/opt/forgerock"            # Folder where the DJ instance will be installed
DJDEST="$DJPARENTDIR/$INSTANCEDIRNAME"   # Folder where the DJ instance will be installed
DJPRESERVE="$DJPARENTDIR/preserved"      # Folder where the replaced DJ instance will be preserved

# Instance Specific Information
ODJFQDN=`hostname -f`                    # FQDN of DJ server
ROOTDN="cn=Directory Manager"            # OpenDJ Administration User
ROOTPASS="b8.HzU5eSy@!c"                      # OpenDJ Admin User's Password
LDAPPORT="3389"                          # LDAP Port on DJ Store
LDAPSPORT="3636"                         # LDAPS Port on DJ Store
ADMINPORT="6444"                         # ADMIN Port on DJ Store
BASEDN="ou=tokens,dc=openam,dc=forgerock,dc=org"    # Root Suffix for DJ Store

# java.properties tuning
JAVADIR="/usr/java/jdk1.8.0_131/jre" # Location of JRE
DEFAULTJAVAHOME=$JAVADIR
STARTDSJAVAARGS="-server -Xms2048M -Xmx2048M -Xmn128M -XX:+UseConcMarkSweepGC -XX:MaxTenuringThreshold=1 -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Xloggc:$DJDEST/logs/gc.log"

# suffix, backend name, backend type, cache percentage (delimited by ^)
declare -a BACKENDS
BACKENDS+=("ou=tokens,dc=openam,dc=forgerock,dc=org^userRoot^je^70")

# Customization File Information
OPENAMCTSPREDATAIMPORT="$ART/config/cts-container.ldif"        # DJ Container Set up for CTS
OPENAMCTSSCHEMA1="$ART/config/cts-add-schema.ldif"             # DJ Schema for OpenAM CTS
OPENAMCTSSCHEMA2="$ART/config/cts-add-multivalue.ldif"         # DJ Schema for OpenAM CTS
OPENAMCTSINDICES1="$ART/config/cts-indices.ldif"               # DJ Indices for OpenAM CTS
OPENAMCTSINDICES2="$ART/config/cts-add-multivalue-indices.ldif" # DJ Indices for OpenAM CTS
POSTIMPORTDATAMODS="$ART/config/add-openAM-CTS-DataModifications.ldif" # Post data changes for OpenAM CTS

#!/bin/bash

shopt -s nocasematch

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


if [ "$#" -eq '1' ]; then
  if [ ! -r "$SCRIPTDIR/artifacts/config/$1" ]; then
    if [ ! -r "$1" ]; then
      echo ""
      echo "ERROR: File $1"
      echo "isn't readable or doesn't exist. Please correct the script argument or"
      echo "restore the properties file and attempt the installation again."
      exit 1
    else
      source $1
    fi
  else
    # read environment specific parameters from properties file
    source $SCRIPTDIR/artifacts/config/$1
  fi
elif [ "$#" -gt '1' ]; then
  echo ""
  echo "ERROR: Too many arguments were provided to this script. Please either"
  echo "specify one properties file name or use no arguments to use the default."
  exit 2
else
  echo ""
  echo "No arguments provided. Attempting to default to the properties file "
  echo "replication.properties"
  if [ ! -r "$SCRIPTDIR/artifacts/config/replication.properties" ]; then
    echo ""
    echo "ERROR: File $SCRIPTDIR/artifacts/config/replication.properties"
    echo "isn't readable or doesn't exist. Please correct the script argument or"
    echo "restore the properties file and attempt the installation again."
    exit 3
  else
    # read environment specific parameters from properties file
    source $SCRIPTDIR/artifacts/config/$1
  fi
fi

if [[ -z ${baseDNs[@]} || -z $DJDIR ]]; then
  echo ""
  echo "ERROR: The properties file provided is missing the array baseDNs or"
  echo "variable DJDIR."
  echo ""
  echo "Please use a replication.properties file with this script."
  exit 1
fi

if [[ ! -r $DJDIR/bin/ldapsearch || ! -r $DJDIR/bin/ldapmodify ]]; then
  echo ""
  echo "ERROR: The properties file provided has an incorrect value set for DJDEST."
  echo "DJDEST should be the path to the directory server's base directory."
  echo ""
  echo "Please correct the value of DJDEST in the replication properties file"
  echo "and run this script again."
  exit 1
fi

ODJFQDN=`hostname -f`


for baseDN in "${baseDNs[@]}"
do
  echo "dn: ou=testRep,$baseDN"
  echo "objectClass: top"
  echo "objectClass: organizationalUnit"
  echo "ou: testRep"
  echo ""
  echo "dn: ou=$ODJFQDN,ou=testRep,$baseDN"
  echo "objectClass: top"
  echo "objectClass: organizationalUnit"
  echo "ou: $ODJFQDN"
  echo ""
done

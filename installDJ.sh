#!/bin/bash 
shopt -s nocasematch
#set -x

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


if [ "$#" -eq '1' ]; then
  if [ ! -r "$SCRIPTDIR/artifacts/config/OpenDJ/$1" ]; then
    echo ""
    echo "ERROR: File $SCRIPTDIR/artifacts/config/OpenDJ/$1"
    echo "isn't readable or doesn't exist. Please correct the script argument or"
    echo "restore the properties file and attempt the installation again."
    exit 1
  else
    # read environment specific parameters from properties file
    source $SCRIPTDIR/artifacts/config/OpenDJ/$1
  fi
elif [ "$#" -gt '1' ]; then
  echo ""
  echo "ERROR: Too many arguments were provided to this script. Please either"
  echo "specify one properties file name or use no arguments to use the default."
  exit 2
else
  echo ""
  echo "No arguments provided. Defaulting to the properties file opendj.properties"
  if [ ! -r "$SCRIPTDIR/artifacts/config/opendj.properties" ]; then
    echo ""
    echo "ERROR: File $SCRIPTDIR/artifacts/config/opendj.properties"
    echo "isn't readable or doesn't exist. Please correct the script argument or"
    echo "restore the properties file and attempt the installation again."
    exit 3
  else
    # read environment specific parameters from properties file
    source $SCRIPTDIR/artifacts/config/OpenDJ/$1
  fi
fi


# Function for searching arrays. Do not alter.
function contains() {
    local q=$#
    local value=${!q}
    for ((c=1;c < $#;c++)) {
        if [[ "${!c}" == "${value}" ]]; then
            echo "y"
            return 0
        fi
    }
    echo "n"
    return 1
}

echo ""
echo "##############################################################################"
echo "#                        INSTALLATION INITIATED!                             #"
echo "##############################################################################"
echo ""


CURRENTUSER=`whoami`
if [[ "$CURRENTUSER" != "$INSTALLUSER" ]]; then
  echo "ERROR:  You are executing this script as: $CURRENTUSER.  To ensure the appropriate permissions"
  echo "you must execute this script as: $INSTALLUSER or change the value of variable INSTALLUSER"
  echo "in opendj.properties to the desired OS username and run this script again."
  echo
  exit 1
fi


echo ""
echo "##############################################################################"
echo "DJ: Checking Availability of Ports: $LDAPPORT and $ADMINPORT"
echo "##############################################################################"
echo "Detecting which tool to use for the port test."

WHICHNC=`which nc > /dev/null; echo $?`

if [ "$WHICHNC" == "0" ]; then
  echo "Using nc to check ports"
  LDAPPORTSTAT=`nc -z localhost $LDAPPORT  > /dev/null; echo $?`
  if [ "$LDAPPORTSTAT" == "0" ]; then
       echo "The requested LDAP port ($LDAPPORT) is not available."
       echo "Exiting installation..."
       exit 1
  else
       echo "Port $LDAPPORT is available"
  fi

  ADMINPORTSTAT=`nc -z localhost $ADMINPORT  > /dev/null; echo $?`
  if [ "$ADMINPORTSTAT" == "0" ]; then
       echo "The requested administration port ($ADMINPORT) is not available."
       echo "Exiting installation..."
       exit 1
  else
       echo "Port $ADMINPORT is available"
  fi
else
  WHICHLSOF=`which lsof > /dev/null; echo $?`

  if [ "$WHICHLSOF" == "0" ]; then
    echo "Using lsof to check ports"
    LDAPPORTSTAT=`lsof -i -P -n | grep LISTEN | grep ":$LDAPPORT "  > /dev/null; echo $?`
    if [ "$LDAPPORTSTAT" == "0" ]; then
       echo "The requested LDAP port ($LDAPPORT) is not available."
       echo "Exiting installation..."
       exit 1
    else
       echo "Port $LDAPPORT is available"
    fi

    ADMINPORTSTAT=`lsof -i -P -n | grep LISTEN | grep ":$ADMINPORT " > /dev/null; echo $?`
    if [ "$ADMINPORTSTAT" == "0" ]; then
       echo "The requested administration port ($ADMINPORT) is not available."
       echo "Exiting installation..."
       exit 1
    else
       echo "Port $ADMINPORT is available"
    fi
  else
    echo "Both nc and lsof are not available on this system. Skipping port check."
  fi
fi

echo ""
echo "##############################################################################"
echo "DJ: Checking Existence of Installation's Parent Folder:  $DJPARENTDIR"
echo "##############################################################################"
if [ ! -d "$DJPARENTDIR" ]; then
     echo "The installation folder's parent folder ($DJPARENTDIR) does not exist."
     echo "Exiting installation..."
     exit 1
else
     echo "The folder ($DJPARENTDIR) exists"
fi


echo ""
echo "##############################################################################"
echo "DJ: Checking Java"
echo "##############################################################################"
if [ ! -x $JAVADIR/bin/java ] ; then
  echo ""
  echo "This server does not have java installed in $JAVADIR."
  echo "Please install Java in that location or update the value of variable JAVADIR"
  echo "in opendj.properties with the correct location."
  exit 1
else
  echo "The java installation ($JAVADIR) exists"
  if [ -z ${OPENDJ_JAVA_HOME:x} ]; then
    echo ""
    echo "OPENDJ_JAVA_HOME is not set for user $INSTALLUSER. OpenDJ 3.5.1 will not"
    echo "work properly without this set."
    echo ""
    if ! `grep -q OPENDJ_JAVA_HOME $HOME/.bashrc`; then
      echo "Updating $HOME/.bashrc."
      echo "export OPENDJ_JAVA_HOME=$JAVADIR" >> $HOME/.bashrc
      echo ""
    fi
    export OPENDJ_JAVA_HOME=$JAVADIR
    SENDREMINDER=1
  fi
fi


echo ""
echo "##############################################################################"
echo "DJ: Extracting OpenDJ binaries into $TMPDIR... "
echo "##############################################################################"
if [ ! -f $ODJSOFT ]; then
  echo ""
  echo "ERROR! The OpenDJ zip file is missing from $ODJSOFT. Please restore the file"
  echo "and begin the installation again."
  exit 1
else
  unzip $ODJSOFT -d $TMPDIR
fi


echo ""
echo "##############################################################################"
echo "DJ: Moving OpenDJ binaries into $DJDEST... "
echo "##############################################################################"
mv $DJSRC $DJDEST/


echo ""
echo "##############################################################################"
echo "DJ: Installing OpenDJ Instance... "
echo "##############################################################################"
if [[ $ODJSOFT == *DS-5.5.0.zip || $ODJSOFT == *DS-5.0.0.zip ]];
then
  $DJDEST/setup --rootUserDN "$ROOTDN" --rootUserPassword $ROOTPASS --hostname $ODJFQDN --ldapPort $LDAPPORT --adminConnectorPort $ADMINPORT --baseDN "$BASEDN" --addBaseEntry --doNotStart --acceptLicense
else
  $DJDEST/setup --cli --no-prompt --rootUserDN "$ROOTDN" --rootUserPassword $ROOTPASS --hostname $ODJFQDN --ldapPort $LDAPPORT --adminConnectorPort $ADMINPORT --baseDN "$BASEDN" --addBaseEntry --doNotStart --acceptLicense
fi

if [[ ! -z $ODJLDAPSPATCH ]];
then
 cd $DJDEST
 rm -fr classes
 unzip $ODJLDAPSPATCH
fi

echo ""
echo ""
echo "##############################################################################"
echo "DJ: Customizing the OpenDJ JVM... "
echo "##############################################################################"
if [[ -z "$STARTDSJAVAARGS" && ! -z "$DEFAULTJAVAHOME" && -r $DJPRESERVE/$INSTANCEDIRNAME/java.properties ]]; then
    echo "Using the JVM settings in $DJPRESERVE/$INSTANCEDIRNAME/java.properties"
    PRESSTARTDSJAVAARGS=`grep ^start-ds $DJPRESERVE/$INSTANCEDIRNAME/java.properties | sed "s#^start-ds.java-args=##"`

    if ! `echo $PRESSTARTDSJAVAARDS | grep --quiet "XX:+PrintGCDetails"`; then
        PRESSTARTDSJAVAARGS="$PRESSTARTDSJAVAARGS -XX:+PrintGCDetails"
    fi

    if ! `echo $PRESSTARTDSJAVAARDS | grep --quiet "XX:+PrintGCTimeStamps"`; then
        PRESSTARTDSJAVAARGS="$PRESSTARTDSJAVAARGS -XX:+PrintGCTimeStamps"
    fi

    if ! `echo $PRESSTARTDSJAVAARDS | grep --quiet "Xloggc"`; then
        PRESSTARTDSJAVAARGS="$PRESSTARTDSJAVAARGS -Xloggc:$DJDEST/logs/gc.log"
    fi

    cat $ART/config/TEMPLATE-java.properties | sed "s#^start-ds.java-args=.*#start-ds.java-args=$PRESSTARTDSJAVAARGS#" | sed "s#^default.java-home=.*#default.java-home=$DEFAULTJAVAHOME#" > $DJDEST/config/java.properties
    if [[ $ODJSOFT == *OpenDJ-3.5.2.zip  ]];
    then
      $DJDEST/bin/dsjavaproperties
    fi
elif [[ -r $ART/config/TEMPLATE-java.properties && ! -z "$STARTDSJAVAARGS" && ! -z "$DEFAULTJAVAHOME" ]]; then
    echo "Using the JVM settings defined in the instance properties file"
    cat $ART/config/TEMPLATE-java.properties | sed "s#^start-ds.java-args=.*#start-ds.java-args=$STARTDSJAVAARGS#" | sed "s#^default.java-home=.*#default.java-home=$DEFAULTJAVAHOME#" > $DJDEST/config/java.properties
    if [[ $ODJSOFT == *OpenDJ-3.5.2.zip  ]];
    then
      $DJDEST/bin/dsjavaproperties
    fi
else
  echo "ERROR! JVM settings are not defined in the opendj.properties file nor"
  echo "are they extracted to $DJPRESERVE/$INSTANCEDIRNAME/java.properties."
  echo "Please address this."
fi
                                                                                                                                 


echo ""
echo "##############################################################################"
echo "DJ: Starting the OpenDJ Instance... "
echo "##############################################################################"
$DJDEST/bin/start-ds


echo ""
echo "##############################################################################"
echo "DJ: Configuring Backends and Suffixes... "
echo "##############################################################################"

declare -a BACKENDTYPES
BACKENDTYPES=("je")

declare -a BACKENDCACHE

for i in "${BACKENDS[@]}"
do
    BASE=`echo $i | cut -f1 -d^`
    DBNAME=`echo $i | cut -f2 -d^`
    DBTYPE=`echo $i | cut -f3 -d^`
    DBCACHEPERCENT=`echo $i | cut -f4 -d^`
  
    if [ $(contains "${BACKENDTYPES[@]}" "$DBTYPE") == "n" ]; then
        echo "Critical error! Backend type $DBTYPE is invalid. Valid backend types are:"
        for $type in "${BACKENDTYPES[@]}"
        do
            echo "  $type"
        done
        echo ""
        echo "Please repair this in opendj.properties before proceeding."
        exit 1
    fi
  
    $DJDEST/bin/dsconfig list-backends --hostname $ODJFQDN --port $ADMINPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --no-prompt --trustAll > /tmp/.backends
  
    if [[ "$BASE" == "$BASEDN" ]]; then
  
        if [[ "$DBNAME" != "userRoot" ]]; then
            echo "Configuration Error! $BASEDN is specified in the BACKENDS array"
            echo "with a backend DB name other than userRoot. Please repair this "
            echo "in opendj.properties before proceeding."
            exit 1
        else
            if [[ "$DBCACHEPERCENT" -ne "50" && ! -z $DBCACHEPERCENT ]]; then
                echo "Setting userRoot's cache percent to $DBCACHEPERCENT and restarting instance"
                USERROOTCACHEPERCENT=$DBCACHEPERCENT
  
                $DJDEST/bin/dsconfig set-backend-prop --backend-name userRoot --set db-cache-percent:$DBCACHEPERCENT --hostname $ODJFQDN --port $ADMINPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS  --no-prompt --trustAll
                $DJDEST/bin/stop-ds
                $DJDEST/bin/start-ds
                echo ""
            fi
        fi
    else
        if [[ "$DBNAME" == "userRoot" && "$BASE" != "$BASEDN" ]]; then
            echo "Adding suffix $BASE to userRoot"
            $DJDEST/bin/dsconfig set-backend-prop --backend-name userRoot --add base-dn:$BASE --hostname $ODJFQDN --port $ADMINPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --no-prompt --trustAll
        fi
  
        if grep --quiet -i "^$DBNAME " /tmp/.backends; then
            if grep -i "^$DBNAME " /tmp/.backends | grep -v -i --quiet " $BASE "; then
                echo "Adding suffix $BASE to existing backend $DBNAME"
                $DJDEST/bin/dsconfig set-backend-prop --backend-name $DBNAME --add base-dn:$BASE --hostname $ODJFQDN --port $ADMINPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --no-prompt --trustAll
            fi
        else
            if [[ "$DBTYPE" == "je" ]]; then
                echo "Creating new backend $DBNAME for suffix $BASE"
                if [[ ! -z $DBCACHEPERCENT ]]; then
                    $DJDEST/bin/dsconfig create-backend --set base-dn:$BASE --set enabled:true --type $DBTYPE --backend-name $DBNAME --set db-cache-percent:$DBCACHEPERCENT --hostname $ODJFQDN --port $ADMINPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --no-prompt --trustAll
                else
                    $DJDEST/bin/dsconfig create-backend --set base-dn:$BASE --set enabled:true --type $DBTYPE --backend-name $DBNAME --hostname $ODJFQDN --port $ADMINPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --no-prompt --trustAll
                fi
            fi
        fi
    fi
done

echo ""
echo "##############################################################################"
echo "DJ: Extending the OpenDJ Schema for OpenAM (Config)..."
echo "##############################################################################"
if [[ ! -z "$OPENAMSCHEMA" && -r "$OPENAMSCHEMA" ]]; then
    $DJDEST/bin/ldapmodify --hostname $ODJFQDN --port $LDAPPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --filename $OPENAMSCHEMA
elif [[ -z "$OPENAMSCHEMA" ]]; then
    echo "DJ:  No OpenAM schema file defined in properties file."
    echo "DJ:  Skipping this step"
elif [[ ! -r "$OPENAMSCHEMA" ]]; then
    echo "DJ: ERROR! OpenAM schema file $OPENAMSCHEMA is defined"
    echo "in the properties file, but it is not readable."
fi

echo ""
echo "##############################################################################"
echo "DJ: Extending the OpenDJ Schema for OpenAM (CTS)..."
echo "##############################################################################"
if [[ ! -z "$OPENAMCTSSCHEMA1" && -r "$OPENAMCTSSCHEMA1" ]]; then
    $DJDEST/bin/ldapmodify --hostname $ODJFQDN --port $LDAPPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --filename $OPENAMCTSSCHEMA1
elif [[ -z "$OPENAMCTSSCHEMA1" ]]; then
    echo "DJ:  No OpenAM CTS schema file defined in properties file."
    echo "DJ:  Skipping this step"
elif [[ ! -r "$OPENAMCTSSCHEMA1" ]]; then
    echo "DJ: ERROR! OpenAM CTS schema file $OPENAMCTSSCHEMA1 is defined"
    echo "in the properties file, but it is not readable."
fi

if [[ ! -z "$OPENAMCTSSCHEMA2" && -r "$OPENAMCTSSCHEMA2" ]]; then
    $DJDEST/bin/ldapmodify --hostname $ODJFQDN --port $LDAPPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --filename $OPENAMCTSSCHEMA2
elif [[ -z "$OPENAMCTSSCHEMA2" ]]; then
    echo "DJ:  No OpenAM CTS schema file defined in properties file."
    echo "DJ:  Skipping this step"
elif [[ ! -r "$OPENAMCTSSCHEMA2" ]]; then
    echo "DJ: ERROR! OpenAM CTS schema file $OPENAMCTSSCHEMA2 is defined"
    echo "in the properties file, but it is not readable."
fi


echo ""
echo "##############################################################################"
echo "DJ: Extending the OpenDJ Schema for OpenAM (Users)..."
echo "##############################################################################"
if [[ ! -z "$OPENAMUSERSCHEMA" && -r "$OPENAMUSERSCHEMA" ]]; then
    $DJDEST/bin/ldapmodify --hostname $ODJFQDN --port $LDAPPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --filename $OPENAMUSERSCHEMA
elif [[ -z "$OPENAMUSERSCHEMA" ]]; then
    echo "DJ:  No OpenAM schema file defined in properties file."
    echo "DJ:  Skipping this step"
elif [[ ! -r "$OPENAMUSERSCHEMA" ]]; then
    echo "DJ: ERROR! OpenAM schema file $OPENAMUSERSCHEMA is defined"
    echo "in the properties file, but it is not readable."
fi


echo ""
echo "##############################################################################"
echo "DJ: Adding Additional Schema..."
echo "##############################################################################"
if [[ ! -z "$ADDITIONALSCHEMA" && -r "$ADDITIONALSCHEMA" ]]; then
  if [[ $ODJSOFT == *DS-5.5.0.zip || $ODJSOFT == *DS-5.0.0.zip ]];
  then
      $DJDEST/bin/ldapmodify --hostname $ODJFQDN --port $LDAPPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --filename $ADDITIONALSCHEMA
  else
      $DJDEST/bin/ldapmodify --hostname $ODJFQDN --port $LDAPPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --defaultAdd --filename $ADDITIONALSCHEMA
  fi
elif [[ -z "$ADDITIONALSCHEMA" ]]; then
    echo "DJ:  No additional schema defined in properties file."
    echo "DJ:  Skipping this step"
elif [[ ! -r "$ADDITIONALSCHEMA" ]]; then
    echo "DJ: ERROR! Additional schema file $ADDITIONALSCHEMA is defined"
    echo "in the properties file, but it is not readable."
fi

echo ""
echo "##############################################################################"
echo "DJ: Extending the OpenDJ Schema for $COMPANY..."
echo "##############################################################################"
if [[ ! -z "$COMPANYSCHEMA" && -r "$COMPANYSCHEMA" ]]; then
    $DJDEST/bin/ldapmodify --hostname $ODJFQDN --port $LDAPPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --filename $COMPANYSCHEMA
elif [[ -z "$COMPANYSCHEMA" ]]; then
    echo "DJ:  No company-specific schema file defined in properties file."
    echo "DJ:  Skipping this step"
elif [[ ! -r "$COMPANYSCHEMA" ]]; then
    echo "DJ: ERROR! Additional schema file $COMPANYSCHEMA is defined"
    echo "in the properties file, but it is not readable."
fi



echo ""
echo "##############################################################################"
echo "DJ: Prepopulating OpenAM (CTS) data..."
echo "##############################################################################"
if [[ ! -z "$OPENAMCTSPREDATAIMPORT" && -r "$OPENAMCTSPREDATAIMPORT" ]]; then
    if [[ $ODJSOFT == *DS-5.5.0.zip || $ODJSOFT == *DS-5.0.0.zip ]];
    then
      $DJDEST/bin/ldapmodify --continueOnError --hostname $ODJFQDN --port $LDAPPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --filename $OPENAMCTSPREDATAIMPORT
    else
      $DJDEST/bin/ldapmodify --continueOnError --hostname $ODJFQDN --port $LDAPPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --defaultAdd --filename $OPENAMCTSPREDATAIMPORT
    fi
elif [[ -z "$OPENAMCTSPREDATAIMPORT" ]]; then
    echo "DJ:  No OpenAM (CTS) prepopulation data defined for this installation."
    echo "DJ:  Skipping this step"
elif [[ ! -r "$OPENAMCTSPREDATAIMPORT" ]]; then
    echo "DJ: ERROR! OpenAM (CTS) prepopulation data is defined"
    echo "in the properties file, but it is not readable."
    echo ""
    echo "Please check the properties file or check $OPENAMCTSPREDATAIMPORT"
fi

echo ""
echo "##############################################################################"
echo "DJ: Adding $COMPANY Specific Policies..."
echo "##############################################################################"
if [[ ! -z "$POLICIES" && -r "$POLICIES" ]]; then
    if [[ $ODJSOFT == *DS-5.5.0.zip || $ODJSOFT == *DS-5.0.0.zip ]];
    then
      $DJDEST/bin/ldapmodify --hostname $ODJFQDN --port $LDAPPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --filename $POLICIES
    else
      $DJDEST/bin/ldapmodify --hostname $ODJFQDN --port $LDAPPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --defaultAdd --filename $POLICIES
    fi
elif [[ -z "$POLICIES" ]]; then
    echo "DJ:  No $COMPANY specific policies file defined in properties file."
    echo "DJ:  Skipping this step"
elif [[ ! -r "$POLICIES" ]]; then
    echo "DJ: ERROR! $COMPANY specific policies file is defined"
    echo "in the properties file, but it is not readable."
    echo ""
    echo "Please check the properties file or check $POLICIES"
fi


echo ""
echo "##############################################################################"
echo "DJ: Adding OpenAM (Config) Specific Indices... "
echo "##############################################################################"
if [[ ! -z "$OPENAMINDICES" && -r "$OPENAMINDICES" ]]; then
    if [[ $ODJSOFT == *DS-5.5.0.zip || $ODJSOFT == *DS-5.0.0.zip ]];
    then
      $DJDEST/bin/ldapmodify --hostname $ODJFQDN --port $LDAPPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --filename $OPENAMINDICES
    else
      $DJDEST/bin/ldapmodify --hostname $ODJFQDN --port $LDAPPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --defaultAdd --filename $OPENAMINDICES
    fi
elif [[ -z "$OPENAMINDICES" ]]; then
    echo "DJ:  No index-specific file defined in properties file."
    echo "DJ:  Skipping this step"
elif [[ ! -r "$OPENAMINDICES" ]]; then
    echo "DJ: ERROR! OpenAM specific indices file is defined"
    echo "in the properties file, but it is not readable."
    echo ""
    echo "Please check the properties file or check $OPENAMINDICES"
fi

echo ""
echo "##############################################################################"
echo "DJ: Adding OpenAM (CTS) Specific Indices... "
echo "##############################################################################"
if [[ ! -z "$OPENAMCTSINDICES1" && -r "$OPENAMCTSINDICES1" ]]; then
    if [[ $ODJSOFT == *DS-5.5.0.zip || $ODJSOFT == *DS-5.0.0.zip ]];
    then
      $DJDEST/bin/ldapmodify --hostname $ODJFQDN --port $LDAPPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --filename $OPENAMCTSINDICES1
    else
      $DJDEST/bin/ldapmodify --hostname $ODJFQDN --port $LDAPPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --defaultAdd --filename $OPENAMCTSINDICES1
    fi
elif [[ -z "$OPENAMCTSINDICES1" ]]; then
    echo "DJ:  No index-specific file defined in properties file."
    echo "DJ:  Skipping this step"
elif [[ ! -r "$OPENAMCTSINDICES1" ]]; then
    echo "DJ: ERROR! OpenAM specific indices file is defined"
    echo "in the properties file, but it is not readable."
    echo ""
    echo "Please check the properties file or check $OPENAMCTSINDICES1"
fi

if [[ ! -z "$OPENAMCTSINDICES2" && -r "$OPENAMCTSINDICES2" ]]; then
    if [[ $ODJSOFT == *DS-5.5.0.zip || $ODJSOFT == *DS-5.0.0.zip ]];
    then
      $DJDEST/bin/ldapmodify --hostname $ODJFQDN --port $LDAPPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --filename $OPENAMCTSINDICES2
    else
      $DJDEST/bin/ldapmodify --hostname $ODJFQDN --port $LDAPPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --defaultAdd --filename $OPENAMCTSINDICES2
    fi
elif [[ -z "$OPENAMCTSINDICES2" ]]; then
    echo "DJ:  No index-specific file defined in properties file."
    echo "DJ:  Skipping this step"
elif [[ ! -r "$OPENAMCTSINDICES2" ]]; then
    echo "DJ: ERROR! OpenAM specific indices file is defined"
    echo "in the properties file, but it is not readable."
    echo ""
    echo "Please check the properties file or check $OPENAMCTSINDICES2"
fi

echo ""
echo "##############################################################################"
echo "DJ: Adding OpenAM (User) Specific Indices... "
echo "##############################################################################"
if [[ ! -z "$OPENAMUSERINDICES" && -r "$OPENAMUSERINDICES" ]]; then
    if [[ $ODJSOFT == *DS-5.5.0.zip || $ODJSOFT == *DS-5.0.0.zip ]];
    then
      $DJDEST/bin/ldapmodify --hostname $ODJFQDN --port $LDAPPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --filename $OPENAMUSERINDICES
    else
      $DJDEST/bin/ldapmodify --hostname $ODJFQDN --port $LDAPPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --defaultAdd --filename $OPENAMUSERINDICES
    fi
elif [[ -z "$OPENAMUSERINDICES" ]]; then
    echo "DJ:  No index-specific file defined in properties file."
    echo "DJ:  Skipping this step"
elif [[ ! -r "$OPENAMUSERINDICES" ]]; then
    echo "DJ: ERROR! OpenAM specific indices file is defined"
    echo "in the properties file, but it is not readable."
    echo ""
    echo "Please check the properties file or check $OPENAMUSERINDICES"
fi

echo ""
echo "##############################################################################"
echo "DJ: Adding $COMPANY Specific Indices... "
echo "##############################################################################"
if [[ ! -z "$INDICES" && -r "$INDICES" ]]; then
    if [[ $ODJSOFT == *DS-5.5.0.zip || $ODJSOFT == *DS-5.0.0.zip ]];
    then
      $DJDEST/bin/ldapmodify --hostname $ODJFQDN --port $LDAPPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --filename $INDICES
    else
      $DJDEST/bin/ldapmodify --hostname $ODJFQDN --port $LDAPPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --defaultAdd --filename $INDICES
    fi
elif [[ -z "$INDICES" ]]; then
    echo "DJ:  No index-specific file defined in properties file."
    echo "DJ:  Skipping this step"                                                                                               
elif [[ ! -r "$OPENAMINDICES" ]]; then
    echo "DJ: ERROR! $COMPANY specific indices file is defined"
    echo "in the properties file, but it is not readable."
    echo ""
    echo "Please check the properties file or check $INDICES"
fi

echo ""
echo "##############################################################################"
echo "DJ: Adding $COMPANY Specific Configuration Changes..."
echo "##############################################################################"
if [[ ! -z "$COMPANYCONFIGMODS" && -r "$COMPANYCONFIGMODS" ]]; then
    $DJDEST/bin/ldapmodify --hostname $ODJFQDN --port $LDAPPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --filename $COMPANYCONFIGMODS
elif [[ -z "$COMPANYCONFIGMODS" ]]; then
    echo "DJ: No $COMPANY specific cn=config changes file defined in properties file."
    echo "DJ: Skipping this step"
elif [[ ! -r "$COMPANYCONFIGMODS" ]]; then
    echo "DJ: ERROR! $COMPANY specific cn=config changes file is defined"
    echo "in the properties file, but it is not readable."
    echo ""
    echo "Please check the properties file or check $COMPANYCONFIGMODS"
fi


echo ""
echo "##############################################################################"
echo "DJ: Stopping Server..."
echo "##############################################################################"
$DJDEST/bin/stop-ds

if [[ -e "$DJPRESERVE/$INSTANCEDIRNAME" ]]; then
    for i in "${BACKENDS[@]}"                                                                                                        
    do                                                                                                                               
        BASE=`echo $i | cut -f1 -d^`                                                                                                 
        DBNAME=`echo $i | cut -f2 -d^`                                                                                               
                                                                                                                                     
        echo ""                                                                                                                      
        echo "##############################################################################"                                        
        echo "DJ: Importing entries into $BASE within backend $DBNAME..."                                                              
        echo "##############################################################################"                                        
                                                                                                                                     
        $DJDEST/bin/import-ldif -F -l $DJPRESERVE/$INSTANCEDIRNAME/$DBNAME-$BASE.ldif -n $DBNAME -b $BASE --rejectFile $DJPRESERVE/$INSTANCEDIRNAME/REJECTS-$DBNAME-$BASE.txt
    done  
else
    echo ""
    echo "##############################################################################"
    echo "DJ: Importing $COMPANY Specific Data (i.e. DIT Structure)..."
    echo "##############################################################################"
    echo "Skipping import of entries because the following directory does not exist:"
    echo "$DJPRESERVE/$INSTANCEDIRNAME"
fi

echo ""
echo "##############################################################################"
echo "DJ: Rebuilding the Indexes for all BaseDNs..."
echo "##############################################################################"
for i in "${BACKENDS[@]}"
do
    echo "Rebuilding indexes for $BASE"
    BASE=`echo $i | cut -f1 -d^`
    if [[ $ODJSOFT == *DS-5.5.0.zip || $ODJSOFT == *DS-5.0.0.zip ]];
    then
        $DJDEST/bin/rebuild-index --baseDN $BASE --rebuildAll --offline
    else
        $DJDEST/bin/rebuild-index --baseDN $BASE --rebuildAll 
    fi
    echo ""
done


echo ""
echo "##############################################################################"
echo "DJ: Starting Server..."
echo "##############################################################################"
$DJDEST/bin/start-ds

echo ""
echo "##############################################################################"
echo "DJ: Post-Import Data Changes..."
echo "##############################################################################"
if [[ ! -z "$POSTIMPORTDATAMODS" && -r "$POSTIMPORTDATAMODS" ]]; then
    $DJDEST/bin/ldapmodify --hostname $ODJFQDN --port $LDAPPORT --bindDN "$ROOTDN" --bindPassword $ROOTPASS --filename $POSTIMPORTDATAMODS
elif [[ -z "$POSTIMPORTDATAMODS" ]]; then
    echo "DJ: No Post-Import data modification file defined in properties file."
    echo "DJ: Skipping this step"
elif [[ ! -r "$POSTIMPORTDATAMODS" ]]; then
    echo "DJ: ERROR! Post-Import data modification file is defined"
    echo "in the properties file, but it is not readable."
    echo ""
    echo "Please check the properties file or check $POSTIMPORTDATAMODS"
fi


echo ""
echo "##############################################################################"
echo "#                          INSTALLATION COMPLETE!                            #"
echo "##############################################################################"

if [ "$SENDREMINDER" == "1" ]; then
    echo ""
    echo ""
    echo "###############################################################################"
    echo "#                                ATTENTION!                                   #"
    echo "###############################################################################"
    echo "This user does not have shell variable OPENDJ_JAVA_HOME configured. This is"
    echo "critical for OpenDJ and OpenDJ tools. This shell variable has been updated"
    echo "in the .bashrc for this user. Please run the following command before"
    echo "proceeding:  source $HOME/.bashrc"
    echo "###############################################################################"
    echo ""
fi


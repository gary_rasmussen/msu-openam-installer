#!/bin/bash


##. ./installAM.properties

INSTALLUSER="frock"                                     # Unix OS User OpenAM Will Run As
FQDN=`hostname -f`
HOST=`hostname`
ENV=`hostname |awk -F- {'print $3'}| awk '{print substr($0,0,3)}'`
ARTA=/home/${INSTALLUSER}/Installer/artifacts/config/OpenAM        # Artifacts directory
ARTB=/home/${INSTALLUSER}/Installer/artifacts/binaries             # Artifacts directory
ARTC=/home/${INSTALLUSER}/Installer/artifacts/config               # Artifacts directory
ARTD=/home/${INSTALLUSER}/Installer/artifacts/config/OpenDJ        # Artifacts directory
LB_PRIMARY_URL=http://iams-am-$ENV.montclair.edu:80/auth
LB_SERVER=iams-am-$ENV.montclair.edu
export JAVAVER=`ls -l /etc/alternatives/java | awk -F\/ {'print $8'}`
JAVA_HOME=/usr/lib/jvm/$JAVAVER/jre
export JAVA_HOME


######################  DEFINING A FUNCTION  ######################################
############################################################


function unconfig {

################  SUPPORT FILES USED IN THIS FUNCTION  ############################
#
#  ~/Installer/unConfigAM.sh
#
###################################################################################

echo ""
echo ""
echo ""
echo "##############################################################################"
echo "##############################################################################"
echo ""
echo " JUST DOUBLE CHECKING THAT YOU WANT TO REMOVE THE CURRENT INSTALLATION"
echo " OF OPENAM AND OPENDJ(IF DJS ARE INSTALLED HERE)"
echo ""
echo "IF YOU ARE 100% SURE YOU WANT TO DO THIS THEN TYPE "YES" : "
echo ""
read -p "ANYTHING ELSE WILL EXIT THE PROGRAM : " OPTIONANSWER
echo "##############################################################################"
echo "##############################################################################"
echo""

if [[ "$OPTIONANSWER" == "YES" ]]
    then
        ~/Installer/unConfigAM.sh
        cp ~/.bash_profile.orig ~/.bash_profile
        cp ~/.bashrc.orig ~/.bashrc
        echo ""
        echo ""
        echo "##############################################################################"
        echo ""
        echo "OK, everything has been removed. "
        echo ""
        echo ""
        echo "##############################################################################"
        echo ""
    else
        echo ""
        echo ""
        echo "##############################################################################"
        echo ""
        echo "You chose not to remove the current installation. Exiting "
        echo ""
        echo ""
        echo "##############################################################################"
        echo ""
        exit
fi

}


######################  DEFINING A FUNCTION  ######################################
############################################################


function OpenDJ {

################  SUPPORT FILES USED IN THIS FUNCTION  ############################
#
#  am-dev-config-opendj.properties
#  am-dev-cts-opendj.properties
#  msu-dev-cts-replication.properties
#  msu-dev-config-replication.properties
#
###################################################################################

echo ""
echo "We are currently working in the $ENV environment"
echo "Are the config and cts directories colocated on this server? "
echo ""
read -p "This is usually only the case in a DEV env. Choose  Y or N : " COLOC
if [ "$COLOC" == "Y" ] || [ "$COLOC" == "y" ]
    then

        echo "##############################################################################"
        echo " Great, we'll begin the directory installation."
        echo " You'll see a couple "Installation Complete" messages "
        echo " but these are just referring to the config and cts OpenDJ installs"
        echo "##############################################################################"
        sleep 10

        cd ~/Installer
        ./installDJ.sh am-dev-config-opendj.properties

        sleep 10

        ./installDJ.sh am-dev-cts-opendj.properties
        sleep 10
        echo ""
        echo ""
        echo "##############################################################################"
        echo "##############################################################################"
        echo ""
        read -p "Are these the first instances of OpenDJ CTS and Config store?  [ Y/N ] : " DJAnswer
        export DJAnswer
        if  [ "$DJAnswer"  == "N" ] || [ "$DJAnswer"  == "n" ]
            then
                echo ""
                echo ""
                echo "##############################################################################"
                echo " I will set up replication now for OpenDJ CTS and Config store "
                echo "##############################################################################"
                ./enableReplication.sh msu-dev-cts-replication.properties
                ./enableReplication.sh msu-dev-config-replication.properties
            else
                echo ""
                echo ""
                echo " OK, no replication will be set up right now "
                echo ""
                echo ""
        fi

    else
    echo "Colocated servers are not selected so none will be installed"
fi

echo ""
echo ""
echo ""
echo "##############################################################################"
echo "#            OpenAM and OpenDJ instances are installed                       #"
echo "##############################################################################"
echo ""
echo ""
echo ""
}


######################  DEFINING A FUNCTION  ######################################
############################################################


function install {

################  SUPPORT FILES USED IN THIS FUNCTION  ############################
#
#  ${ARTB}/apache-tomcat-8.5.27.zip
#  ${ARTB}/AM-5.5.1.zip
#  ${ARTB}/AM-6.0.0.2.zip
#  ${ARTA}/setenv.sh
#
###################################################################################

mkdir /home/frock/software
cd /home/frock/software
unzip ${ARTB}/apache-tomcat-8.5.27.zip
mv /home/frock/software/apache-tomcat-8.5.27 /opt/forgerock/tomcat

read -p " Select the OpenAM version to install: (1) AM-5.5.1 (2) AM-6.0.0.2 : " OAMV
if [[ "$OAMV" == "1" ]]; then
    echo "Installing AM-5.5.1 "
    cd /home/frock/software
    unzip ${ARTB}/AM-5.5.1.zip
    cp /home/frock/software/openam/AM-5.5.1.war /opt/forgerock/tomcat/webapps/auth.war

elif [[ "$OAMV" == "2" ]]; then
    echo    "Installing AM-6.0.0.2 "
    cd /home/frock/software
    unzip ${ARTB}/AM-6.0.0.2.zip
    cp /home/frock/software/openam/AM-6.0.0.2.war /opt/forgerock/tomcat/webapps/auth.war
else
    echo " Not a valid response. "
    exit
fi

mkdir -p /home/frock/tools
mkdir -p /home/frock/tools/configurator
cd /home/frock/tools/configurator
unzip /home/frock/software/openam/AM-SSOConfiguratorTools-*.zip

cp ${ARTB}/MSU-$ENV-keystore.jceks /opt/forgerock/config/auth/keystore.jceks
cp ${ARTB}/MSU-$ENV-keystore.jks /opt/forgerock/config/auth/keystore.jks
cp ${ARTB}/.MSU-$ENV-keypass /opt/forgerock/config/auth/.keypass
cp ${ARTB}/.MSU-$ENV-storepass /opt/forgerock/config/auth/.storepass


cp ${ARTA}/setenv.sh /opt/forgerock/tomcat/bin/

CONFIGURATORV=`ls /home/frock/tools/configurator | grep configurator`

chmod 750 /opt/forgerock/tomcat/bin/*.sh
/opt/forgerock/tomcat/bin/startup.sh

}


######################  DEFINING A FUNCTION  ######################################
############################################################


function config {

################  SUPPORT FILES USED IN THIS FUNCTION  ############################
#
#  ${ARTA}/am-dev1-msu-configuration-baseline
#  ${ARTA}/am-dev1-msu-configuration
#  ${ARTA}/am-dev2-msu-configuration
#
###################################################################################

echo ""
echo ""
echo ""
echo "##############################################################################"
echo "#            Starting the OpenAM configurator section                        #"
echo "##############################################################################"
echo ""
echo ""
echo ""
cd /home/frock/tools/configurator




echo ""
echo ""
export CONFIGNUMB

##if [[ "$CONFIGNUMB" == "1" ]]
##	then
	echo "##########################################################################"
	echo ""
	echo " A baseline install uses the embedded OpenDJ for Config, User, and CTS "
	echo "  even if you have installed external DJs. It is used for basic setup and "
	echo "  testing, and is not meant for production"
	echo ""
	echo "##########################################################################"
	echo ""
	read -p "Is this a baseline install? Y/N :  " BASEIN
	echo ""
	echo "##########################################################################"
	echo ""
		if [ "$BASEIN" == "Y" ] || [ "$BASEIN" == "y" ] 
		then
echo "Baseline install"
sleep 5
    		sed -e s/FQDN/$FQDN/g ${ARTA}/am-$ENV-msu-configuration-baseline.TMPL > ${ARTA}/am-$ENV-msu-configuration-baseline
		cp ${ARTA}/am-$ENV-msu-configuration-baseline /home/frock/tools/configurator/
		java -jar ${CONFIGURATORV} --file am-$ENV-msu-configuration-baseline
	else
echo "NOT Baseline install"
sleep 5

	    sed -e s/FQDN/$FQDN/g ${ARTA}/am-$ENV-msu-configuration.TMPL > ${ARTA}/am-$ENV-msu-configuration

		cp ${ARTA}/am-$ENV-msu-configuration /home/frock/tools/configurator/
		java -jar ${CONFIGURATORV} --file am-$ENV-msu-configuration
	fi

##else
##echo "##############################################################################"
##echo "##############################################################################"
##echo "The number you have selected is invalid and the configuration has failed. "
##echo "You will need to run this installer again and select Config"
##echo "##############################################################################"
##echo "##############################################################################"
##exit 3
##fi

echo "Sleeping for 10 seconds to watch for errors "
sleep 10
}


######################  DEFINING A FUNCTION  ######################################
############################################################


function Ssoadm {

################  SUPPORT FILES USED IN THIS FUNCTION  ############################
#
#  ${ARTB}/AM-SSOAdminTools-5.1.1.1.zip
#  ${ARTB}/AM-SSOAdminTools-5.1.1.3.zip
#  ${ARTB}/SSOADMconfiged.tgz
#  ${ARTA}/tools-passwdfile
#
###################################################################################

export JAVAVER=`ls -l /etc/alternatives/java | awk -F\/ {'print $8'}`
JAVA_HOME=/usr/lib/jvm/$JAVAVER/jre
export JAVA_HOME

cd /home/frock/tools
##unzip ${ARTB}/AM-SSOAdminTools-5.1.1.1.zip ## AM-5 version of ssoadm
##unzip ${ARTB}/AM-SSOAdminTools-5.1.1.3.zip ## AM-6 version of ssoadm
tar -xzvf ${ARTB}/SSOADMconfiged.tgz
sed -i s/FQDN/$FQDN/g ~/tools/ssoadmintool/auth/bin/ssoadm
sed -i s/LB_SERVER/${LB_SERVER}/g ~/tools/ssoadmintool/auth/bin/ssoadm
sed -i s/JAVAVER/${JAVAVER}/g ~/tools/ssoadmintool/auth/bin/ssoadm

cp ${ARTA}/tools-passwdfile /home/frock/tools/passwdfile
chmod 400 /home/frock/tools/passwdfile

echo ""
echo ""
echo ""
echo "##############################################################################"
echo "#               Runing an ssoadm test to verify it was installed correctly   #"
echo "##############################################################################"
echo ""
echo ""
echo ""

export JAVAVER=`ls -l /etc/alternatives/java | awk -F\/ {'print $8'}`
JAVA_HOME=/usr/lib/jvm/$JAVAVER/jre
export JAVA_HOME

~/tools/ssoadmintool/auth/bin/ssoadm list-servers -u amadmin -f /home/frock/tools/passwdfile

echo ""
echo ""
echo ""
echo "##############################################################################"
echo "#               The current server list should have been displayed           #"
echo "##############################################################################"
echo ""
echo ""
echo ""
sleep 10
}


######################  DEFINING A FUNCTION  ######################################
############################################################


function customize {

ARTD=/home/frock/Installer/artifacts           ##  Artifacts directory


if [ "$ENV" != "dev" ]
    then
        if [[ "$HOST" = *"1"* ]] || [[ "$HOST" = *"2"* ]]
            then
                CTSLIST=$CTSSVR1:6389,$CTSSVR2:6389
                sed -e s/CTSLIST/$CTSLIST/g ${ARTA}/am-cts-configuration.TMPL > ${ARTA}/am-cts-configuration.$ENV
            else
                CTSLIST=$CTSSVR2:6389,$CTSSVR1:6389
                sed -e s/CTSLIST/$CTSLIST/g ${ARTA}/am-cts-configuration.TMPL > ${ARTA}/am-cts-configuration.$ENV
        fi
    else
        if [[ "$HOST" = *"1"* ]]
            then
                CTSLIST=$CTSSVR1:6389,$CTSSVR2:6389
                sed -e s/CTSLIST/$CTSLIST/g ${ARTA}/am-cts-configuration.TMPL > ${ARTA}/am-cts-configuration.$ENV
                echo ""
                echo " We'll use the the "${ARTA}/am-cts-configuration.$ENV " file for configuration."
                echo " Check it out below:"
                head -15 ${ARTA}/am-cts-configuration.$ENV
                sleep 30
            else
                CTSLIST=$CTSSVR2:6389,$CTSSVR1:6389
                sed -e s/CTSLIST/$CTSLIST/g ${ARTA}/am-cts-configuration.TMPL > ${ARTA}/am-cts-configuration.$ENV
                echo ""
                echo " We'll use the the "${ARTA}/am-cts-configuration.$ENV " file for configuration."
                echo " Check it out below:"
                head -15 ${ARTA}/am-cts-configuration.$ENV
                sleep 30
        fi
fi


################  SUPPORT FILES USED IN THIS FUNCTION  ############################
#
#  ${ARTA}/identitystore-datafile.dev
#  ${ARTA}/am-cts-configuration.dev
#  ~/.bash_profile
#  ~/.bashrc
#  
###################################################################################

echo "##############################################################################"
echo " We only run this customization on the first server in the environment."
echo ""
read -p "Select which type of server this is in the enviroment (1) First (2) for additional servers :" CONFIGNUMB 
export CONFIGNUMB
echo "##############################################################################"

if [[ "$CONFIGNUMB" == "1" ]]
    then

        SSOADM=/home/frock/tools/ssoadmintool/auth/bin/ssoadm
        SSOADMPWORD=/home/frock/tools/passwdfile

        echo "#################    Adding in the MSU realm now    ###########"

        echo ${SSOADM} " create-realm --realm /msu -u amadmin -f "${SSOADMPWORD}

        ${SSOADM} create-realm --realm /msu -u amadmin -f ${SSOADMPWORD}

        echo "#################    Adding the external Userstore    #################"
        echo ${SSOADM} " create-datastore -e msu -u amadmin -f "${SSOADMPWORD}"  -m DJDevUserStore -t LDAPv3ForOpenDS -D "${ARTA}"/identitystore-datafile."$ENV

        ${SSOADM} create-datastore -e msu -u amadmin -f ${SSOADMPWORD}  -m DJDevUserStore -t LDAPv3ForOpenDS -D ${ARTA}/identitystore-datafile.$ENV

        echo "#########    Removing the internal Userstore               ###############"
        echo ${SSOADM} "  delete-datastores -e msu -m embedded -u amadmin -f "${SSOADMPWORD}

        ${SSOADM}  delete-datastores -e msu -m embedded -u amadmin -f ${SSOADMPWORD}

        echo "########    Making the external CTS the default               ###########"
        echo ${SSOADM} "  update-server-cfg -s default -u  amadmin -f "${SSOADMPWORD} " -D "${ARTA}"/am-cts-configuration."$ENV

        ${SSOADM}  update-server-cfg -s default -u  amadmin -f ${SSOADMPWORD}  -D ${ARTA}/am-cts-configuration.$ENV

        echo "#######    Setting Session Cookie Name                       ###########"
        echo ${SSOADM} "  update-server-cfg -s default -u amadmin -f "${SSOADMPWORD}"   -a com.iplanet.am.cookie.name=msuSSOToken-"$ENV

        ${SSOADM}  update-server-cfg -s default -u amadmin -f ${SSOADMPWORD}   -a com.iplanet.am.cookie.name=msuSSOToken-$ENV

        echo "########    Creating the LDAP Authentication Chain            ###########"
        echo ${SSOADM} "  create-auth-cfg -u amadmin -f "${SSOADMPWORD}"   -e msu -m msuLDAP"

        ${SSOADM}  create-auth-cfg -u amadmin -f ${SSOADMPWORD}   -e msu -m msuLDAP

        echo "########    Creating the LDAP Authentication Modules          ###########"
        echo ${SSOADM} "  create-auth-instance -u amadmin -f "${SSOADMPWORD}"  -e msu -m LDAP -t OATH"

        ${SSOADM}  create-auth-instance -u amadmin -f "${SSOADMPWORD}"  -e msu -m LDAP -t OATH"

        echo "########  Adding the LDAP Authentication Modules to the LDAP Chain  ############"
        echo ${SSOADM} "  add-auth-cfg-entr -u amadmin -f "${SSOADMPWORD}"   -e msu -m msuMFAChain -o LDAP -c REQUIRED"

        ${SSOADM}  add-auth-cfg-entr -u amadmin -f ${SSOADMPWORD}   -e msu -m msuMFAChain -o LDAP -c REQUIRED

        echo "########    Creating the msuMFAChain Authentication Chain      ###########"
        echo ${SSOADM} "  create-auth-cfg -u amadmin -f "${SSOADMPWORD} "  -e msu -m msuMFAChain"

        ${SSOADM}  create-auth-cfg -u amadmin -f ${SSOADMPWORD}   -e msu -m msuMFAChain

        echo "#########    Creating the msuMFAChain Authentication Modules    ###########"
        echo ${SSOADM} "  create-auth-instance -u amadmin -f "${SSOADMPWORD}"  -e msu -m ForgeRockAuthenticator -t OATH"
        echo ${SSOADM} "  create-auth-instance -u amadmin -f "${SSOADMPWORD}"  -e msu -m Adaptive -t Adaptive"

        ${SSOADM}  create-auth-instance -u amadmin -f ${SSOADMPWORD}  -e msu -m ForgeRockAuthenticator -t OATH
        ${SSOADM}  create-auth-instance -u amadmin -f ${SSOADMPWORD}  -e msu -m Adaptive -t Adaptive


        echo "########  Adding the Authentication Modules to the msuMFAChain  #######"
        echo ${SSOADM} "  add-auth-cfg-entr -u amadmin -f "${SSOADMPWORD}"   -e msu -m msuMFAChain -o LDAP -c REQUISITE"
        echo ${SSOADM} "  add-auth-cfg-entr -u amadmin -f "${SSOADMPWORD}"   -e msu -m msuMFAChain -o Adaptive -c SUFFICIENT"
        echo ${SSOADM} "  add-auth-cfg-entr -u amadmin -f "${SSOADMPWORD}"   -e msu -m msuMFAChain -o ForgeRockAuthenticator -c REQUIRED"

        ${SSOADM}  add-auth-cfg-entr -u amadmin -f ${SSOADMPWORD}   -e msu -m msuMFAChain -o LDAP -c REQUISITE
        ${SSOADM}  add-auth-cfg-entr -u amadmin -f ${SSOADMPWORD}   -e msu -m msuMFAChain -o Adaptive -c SUFFICIENT
        ${SSOADM}  add-auth-cfg-entr -u amadmin -f ${SSOADMPWORD}   -e msu -m msuMFAChain -o ForgeRockAuthenticator -c REQUIRED

        echo "########  Setting session values for timeouts, max sessions, etc #####"
        echo ${SSOADM}  "add-svc-realm -s iPlanetAMSessionService -u amadmin -f "${SSOADMPWORD}  "  -e msu -a iplanet-am-session-max-session-time=120"
        echo ${SSOADM}  "add-svc-realm -s iPlanetAMSessionService -u amadmin -f "${SSOADMPWORD}  "  -e msu -a iplanet-am-session-max-idle-time=30"
        echo ${SSOADM}  "add-svc-realm -s iPlanetAMSessionService -u amadmin -f "${SSOADMPWORD}  "  -e msu -a iplanet-am-session-max-caching-time=3"
        echo ${SSOADM}  "add-svc-realm -s iPlanetAMSessionService -u amadmin -f "${SSOADMPWORD}  "  -e msu -a iplanet-am-session-quota-limit=5"

        ${SSOADM}  add-svc-realm -s iPlanetAMSessionService -u amadmin -f ${SSOADMPWORD}  -e msu -a iplanet-am-session-max-session-time=120
        ${SSOADM}  add-svc-realm -s iPlanetAMSessionService -u amadmin -f ${SSOADMPWORD}  -e msu -a iplanet-am-session-max-idle-time=30
        ${SSOADM}  add-svc-realm -s iPlanetAMSessionService -u amadmin -f ${SSOADMPWORD}  -e msu -a iplanet-am-session-max-caching-time=3
        ${SSOADM}  add-svc-realm -s iPlanetAMSessionService -u amadmin -f ${SSOADMPWORD}  -e msu -a iplanet-am-session-quota-limit=5

        echo "######## DONE  Setting session values #################################"

        export JAVAVER=`ls -l /etc/alternatives/java | awk -F\/ {'print $8'}`
        JAVA_HOME=/usr/lib/jvm/$JAVAVER/jre
        export JAVA_HOME

        cp ~/.bash_profile ~/.bash_profile.orig
        echo "JAVA_HOME=/usr/lib/jvm/$JAVAVER/jre ">> ~/.bash_profile
        echo "export JAVA_HOME" >> ~/.bash_profile

        cp ~/.bashrc ~/.bashrc.orig
        echo "JAVA_HOME=/usr/lib/jvm/$JAVAVER/jre ">> ~/.bashrc
        echo "export JAVA_HOME" >> ~/.bashrc

        echo "### All Done. Your server is now setup, configured, and customized for MSU ####"

    else
        echo "##############################################################################"
        echo "You selected (2) additional server. Customization does not get run on additional servers"
        echo "therefore we will not be performing any additional tasks."
        echo "##############################################################################"

        exit 1
fi

}



#################################################################
#
#           THIS IS THE MAIN INSTALLER PROGRAM
#
#################################################################

clear
clear

. ./installAM.properties

if [ "$ENV" == "dev" ]; then
        echo "This is the DEV environment "
        AMHOSTID=am
        CFGHOSTID=am                          ##  OpenDJ Config Store (colocated)
        CTSHOSTID=am                          ##  OpenDJ Token(session) Store (colocated)

   elif [ "$ENV" == "tst" ]; then
        echo "This is the TEST environment "
        AMHOSTID=am
        CFGHOSTID=djcs                          ##  OpenDJ Config Store
        CTSHOSTID=djss                          ##  OpenDJ Token(session) Store


   elif [ "$ENV" == "prd" ]; then
        echo "This is the PROD environment "
        AMHOSTID=am
        CFGHOSTID=djcs                          ##  OpenDJ Config Store
        CTSHOSTID=djss                          ##  OpenDJ Token(session) Store


fi


ARTA=/home/${INSTALLUSER}/Installer/artifacts/config/OpenAM        # Artifacts directory
ARTB=/home/${INSTALLUSER}/Installer/artifacts/binaries             # Artifacts directory
ARTC=/home/${INSTALLUSER}/Installer/artifacts/config               # Artifacts directory
ARTD=/home/${INSTALLUSER}/Installer/artifacts/config/OpenDJ        # Artifacts directory

AMSVR1=iams-am-${ENV}1.admsu.montclair.edu
AMSVR2=iams-am-${ENV}2.admsu.montclair.edu
AMSVR3=iams-am-${ENV}3.admsu.montclair.edu
AMSVR4=iams-am-${ENV}4.admsu.montclair.edu

CTSSVR1=iams-${CTSHOSTID}-${ENV}1.admsu.montclair.edu
CTSSVR2=iams-${CTSHOSTID}-${ENV}2.admsu.montclair.edu

CFGSVR1=iams-${CFGHOSTID}-${ENV}1.admsu.montclair.edu
CFGSVR2=iams-${CFGHOSTID}-${ENV}2.admsu.montclair.edu



##echo $AMHOSTID  $CFGHOSTID  $CTSHOSTID $AMSVR1 $AMSVR2 $CTSSVR1 $CTSSVR2 $CFGSVR1 $CFGSVR2



CURRENTUSER=`whoami`
if [[ "$CURRENTUSER" != "${INSTALLUSER}" ]]
    then
      echo "ERROR:  You are executing this script as: $CURRENTUSER.  To ensure the appropriate permissions"
      echo "you must execute this script as: ${INSTALLUSER} or change the value of variable INSTALLUSER"
      echo "in installAM.properties to the desired OS username and run this script again."
      echo
      exit 1
fi

echo "##############################################################################"
echo "##############################################################################"
echo ""
echo ""
echo "I see that based on the server name you are working in the $ENV environment"
echo ""
read -p "If this is correct and your would like to continue type "Y" or "YES" : " LETSGO
    if [ "$LETSGO" == "YES" ] || [ "$LETSGO" == "Y" ]
        then
            echo " Great we will continue on in the $ENV environment  "
        else
            echo "exiting the script"
            exit 1

        fi
echo ""
echo ""
echo "##############################################################################"
echo "##############################################################################"
echo ""
echo ""
echo "Will you be installing for the first time, configuring due to "
echo "previous invalid selection, adding customizations such as realms, or external stores"
echo "or need to blow away and do a full install ?"
echo ""
echo "Choose your selection number: "
echo ""
echo "(1) New install and basic configuration of OpenAM including collocated OpenDJ if desired"
echo "(2) Perform the basic configuration of OpenAM"
echo "(3) Perform the Customization of OpenAM (realms, external datastores, etc)"
echo "(4) Perform a complete removal of the entire installation and complete install, config, customize"
echo ""
read -p "Your Reply?  " WHAT2DO

############################################################

if [[ "$WHAT2DO" == "1" ]]
    then
        echo "##############################################################################"
        echo "##############################################################################"
        echo "You choose (1)  New install and configure"
        echo ""
        echo "IF YOU ARE 100% SURE YOU WANT TO DO THIS THEN TYPE "YES".  "
        echo ""
            read -p "ANYTHING ELSE WILL EXIT THE PROGRAM :" OPTIONANSWER
        echo "##############################################################################"
        echo "##############################################################################"
        echo""

    if [[ "$OPTIONANSWER" == "YES" ]]
        then
            OpenDJ
            install
                sleep 30
            config
            Ssoadm
        else
            echo "exiting the script"
    fi

    exit 1

############################################################

    elif [[ "$WHAT2DO" == "2" ]]
        then
            echo "##############################################################################"
            echo "##############################################################################"
            echo " You choose (2) Perform the initial configuration of OpenAM"
            echo ""
            echo "IF YOU ARE 100% SURE YOU WANT TO DO THIS THEN TYPE "YES".  "
            echo ""
            read -p "ANYTHING ELSE WILL EXIT THE PROGRAM :" OPTIONANSWER
            echo "##############################################################################"
            echo "##############################################################################"
            echo""

    if [[ "$OPTIONANSWER" == "YES" ]]
        then
            config
            Ssoadm
        else
            echo "exiting the script"
    fi

    exit 2

############################################################

    elif [[ "$WHAT2DO" == "3" ]]
        then
            echo""
            echo""
            echo "##############################################################################"
            echo "##############################################################################"
            echo " You choose (3) Perform the Customization of OpenAM (realms, external datastores, etc)"
            echo ""
            echo "Only perform this task if this is the FIRST installation of the environment"
            echo "If this is the first installation of the environment then type "YES""
            echo ""
            read -p "ANYTHING ELSE WILL EXIT THE PROGRAM :" OPTIONANSWER
            echo "##############################################################################"
            echo "##############################################################################"
            echo""
            echo""
            echo""

    if [[ "$OPTIONANSWER" == "YES" ]]
        then
            customize
        else
            echo "exiting the script"
    fi

    exit 3

############################################################

elif [[ "$WHAT2DO" == "4" ]]
  then
    echo "##############################################################################"
    echo "##############################################################################"
    echo " You choose (4) Perform a complete removal of the entire installation and start over."
    echo ""
    echo "IF YOU ARE 100% SURE YOU WANT TO DO THIS THEN TYPE "YES"."
    echo ""
  read -p "ANYTHING ELSE WILL EXIT THE PROGRAM :" OPTIONANSWER
    echo "##############################################################################"
    echo "##############################################################################"
    echo""
    echo""

if [[ "$OPTIONANSWER" == "YES" ]]
  then
    unconfig
    OpenDJ
    install
        sleep 30
    config
    Ssoadm
    customize

echo "exiting the script"
fi
exit 4
fi

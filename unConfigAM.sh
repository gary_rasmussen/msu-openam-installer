#!/bin/bash


/opt/forgerock/tomcat/bin/shutdown.sh

echo "############################################################################"
echo "#  If you recieve the below message it means the server was not running    #"
echo "#                                                                          #"
echo "#   SEVERE: Catalina.stop:                                                 #"
echo "#   java.net.ConnectException: Connection refused (Connection refused)     #"
echo "#                                                                          #"
echo "############################################################################"
sleep 15
kill -9 `ps -ef | grep tomcat|grep java| awk '{print $2}'`

cp ~/.bash_profile.orig ~/.bash_profile
cp ~/.bashrc.orig ~/.bashrc
rm -rf /opt/forgerock/auth
rm -rf /home/frock/.openamcfg 
rm -rf /home/frock/tools
rm -rf /home/frock/software
rm -rf /home/frock/install.log
rm -rf /opt/forgerock/tomcat
rm -rf /opt/forgerock/config

cd /home/frock/Installer
./removeDJ.sh am-dev-config-opendj.properties
./removeDJ.sh am-dev-cts-opendj.properties


#!/bin/bash

shopt -s nocasematch

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


if [ "$#" -eq '1' ]; then
  if [ ! -r "$SCRIPTDIR/artifacts/config/OpenDJ/$1" ]; then
    if [ ! -r "$1" ]; then
      echo ""
      echo "ERROR: File $1"
      echo "isn't readable or doesn't exist. Please correct the script argument or"
      echo "restore the properties file and attempt the installation again."
      exit 1
    else
      source $1
    fi
  else
    # read environment specific parameters from properties file
    source $SCRIPTDIR/artifacts/config/OpenDJ/$1
  fi
elif [ "$#" -gt '1' ]; then
  echo ""
  echo "ERROR: Too many arguments were provided to this script. Please either"
  echo "specify one properties file name or use no arguments to use the default."
  exit 2
else
  echo ""
  echo "No arguments provided. Attempting to default to the properties file "
  echo "replication.properties"
  if [ ! -r "$SCRIPTDIR/artifacts/config/replication.properties" ]; then
    echo ""
    echo "ERROR: File $SCRIPTDIR/artifacts/config/replication.properties"
    echo "isn't readable or doesn't exist. Please correct the script argument or"
    echo "restore the properties file and attempt the installation again."
    exit 3
  else
    # read environment specific parameters from properties file
    source $SCRIPTDIR/artifacts/config/OpenDJ/$1
  fi
fi

if [[ -z $DJDIR || -z $initHost || -z ${newHosts[@]} || -z $adminPort || -z $repPort || -z ${baseDNs[@]} || -z $bindPass || -z $adminPass ]]; then
  echo ""
  echo "ERROR: The properties file provided is missing one or more of the following"
  echo "variables: \$DJDIR, \$initHost, @newHosts, \$adminPort, \$repPort, @baseDNs, "
  echo "\$bindPass, \$adminPass"
  echo ""
  echo "Please use a replication.properties file with this script."
  exit 1
fi

if [[ ! -r $DJDIR/bin/dsconfig || ! -r $DJDIR/bin/dsreplication ]]; then
  echo ""
  echo "ERROR: The properties file provided has an incorrect value set for DJDEST."
  echo "DJDEST should be the path to the directory server's base directory."
  echo ""
  echo "Please correct the value of DJDEST in the replication properties file"
  echo "and run this script again."
  exit 1
fi

set -u

# $DSCONFIG is the path to the dsconfig command. 
#
# DO NOT MODIFY THIS VARIABLE!
DSCONFIG=$DJDIR/bin/dsconfig


# $DSREPLICATION is the path to the dsreplication command. 
#
# DO NOT MODIFY THIS VARIABLE!
DSREPLICATION=$DJDIR/bin/dsreplication   

# $COUNT is a variable that prints the number preceeding each step
# to enable ease of tracking each step. 
#
# DO NOT MODIFY THIS VARIABLE!
COUNT=1

# Do not modify the next four variables.
initPurgeDelay=0
initGroupID=0
initSchemaGroupId=0
initAdminGroupId=0

# Function for searching arrays. Do not alter.
function contains() {
    local n=$#
    local value=${!n}
    for ((i=1;i < $#;i++)) {
        if [[ "${!i}" == "${value}" ]]; then
            echo "y"
            return 0
        fi
    }
    echo "n"
    return 1
}


# Main Program
for newHost in "${newHosts[@]}"
do

  echo "################################################################"
  echo "# Commands for connecting $newHost"
  echo "# to $initHost"
  echo "################################################################"
  echo ""

  for baseDN in "${baseDNs[@]}"
  do
    echo "###### Commands for baseDN $baseDN ######"
    echo
    echo "# Step $COUNT: Enable Replication"
    if [ $SHOWPASS -eq 1 ]; then
      echo "$DSREPLICATION enable --host1 $initHost --port1 $adminPort --bindDN1 \"cn=Directory Manager\" --bindPassword1 '$bindPass' --replicationPort1 $repPort --host2 $newHost --port2 $adminPort2 --bindDN2 \"cn=Directory Manager\" --bindPassword2 '$bindPass' --replicationPort2 $repPort2 --adminUID admin --adminPassword \"$adminPass\" --baseDN \"$baseDN\" -n -X"
    else
      echo "$DSREPLICATION enable --host1 $initHost --port1 $adminPort --bindDN1 \"cn=Directory Manager\" --bindPassword1 XXXXXXXXXX --replicationPort1 $repPort --host2 $newHost --port2 $adminPort2 --bindDN2 \"cn=Directory Manager\" --bindPassword2 XXXXXXXXXX --replicationPort2 $repPort2 --adminUID admin --adminPassword XXXXXXXXXX --baseDN \"$baseDN\" -n -X"
    fi
    echo "Preparing to execute Step $COUNT"
    read -p "Execute? [y/s/q]: " ANS
    if [[ "$ANS" == "y" ]]; then
      $DSREPLICATION enable --host1 $initHost --port1 $adminPort --bindDN1 "cn=Directory Manager" --bindPassword1 "$bindPass" --replicationPort1 $repPort --host2 $newHost --port2 $adminPort2 --bindDN2 "cn=Directory Manager" --bindPassword2 "$bindPass" --replicationPort2 $repPort2 --adminUID admin --adminPassword "$adminPass" --baseDN "$baseDN" -n -X
    elif [[ "$ANS" == "s" ]]; then
      echo "Skipping command"
      echo
    else
      echo "Quitting at step $COUNT."
      exit 1
    fi
    COUNT=$((COUNT+1))


    if [ "$groupId" -ne 1 ]; then
      echo
      echo "# Step $COUNT: Set the Group ID of $baseDN on $initHost to $groupId"
      if [ $SHOWPASS -eq 1 ]; then
        echo "$DSCONFIG set-replication-domain-prop --provider-name \"Multimaster Synchronization\" --domain-name \"$baseDN\" --set group-id:$groupId --hostname $initHost --port $adminPort --bindDN \"cn=Directory Manager\" --bindPassword  -n -X"
      else
        echo "$DSCONFIG set-replication-domain-prop --provider-name \"Multimaster Synchronization\" --domain-name \"$baseDN\" --set group-id:$groupId --hostname $initHost --port $adminPort --bindDN \"cn=Directory Manager\" --bindPassword XXXXXXXXXX -n -X"
      fi
      echo "Preparing to execute Step $COUNT"
      read -p "Execute? [y/s/q]: " ANS
      if [[ "$ANS" == "y" ]]; then
        $DSCONFIG set-replication-domain-prop --provider-name "Multimaster Synchronization" --domain-name "$baseDN" --set group-id:$groupId --hostname $initHost --port $adminPort --bindDN "cn=Directory Manager" --bindPassword "$bindPass" -n -X
      elif [[ "$ANS" == "s" ]]; then
        echo "Skipping command"
        echo
      else
        echo "Quitting at step $COUNT."
        exit 1
      fi
      COUNT=$((COUNT+1))

      echo
      echo "# Step $COUNT: Set the Group ID of $baseDN on $newHost to $groupId"
      if [ $SHOWPASS -eq 1 ]; then
        echo "$DSCONFIG set-replication-domain-prop --provider-name \"Multimaster Synchronization\" --domain-name \"$baseDN\" --set group-id:$groupId --hostname $newHost --port $adminPort2 --bindDN \"cn=Directory Manager\" --bindPassword '$bindPass' -n -X"
      else
        echo "$DSCONFIG set-replication-domain-prop --provider-name \"Multimaster Synchronization\" --domain-name \"$baseDN\" --set group-id:$groupId --hostname $newHost --port $adminPort2 --bindDN \"cn=Directory Manager\" --bindPassword XXXXXXXXXX -n -X"
      fi
      echo "Preparing to execute Step $COUNT"
      read -p "Execute? [y/s/q]: " ANS
      if [[ "$ANS" == "y" ]]; then
        $DSCONFIG set-replication-domain-prop --provider-name "Multimaster Synchronization" --domain-name "$baseDN" --set group-id:$groupId --hostname $newHost --port $adminPort2 --bindDN "cn=Directory Manager" --bindPassword "$bindPass" -n -X
      elif [[ "$ANS" == "s" ]]; then
        echo "Skipping command"
        echo
      else
        echo "Quitting at step $COUNT."
        exit 1
      fi
      COUNT=$((COUNT+1))
    fi

    echo
    echo "# Step $COUNT: Populate $newHost with data from $initHost"
    if [ $SHOWPASS -eq 1 ]; then
      echo "$DSREPLICATION initialize --baseDN \"$baseDN\" --adminUID admin --adminPassword \"$adminPass\" --hostSource $initHost --portSource $adminPort --hostDestination $newHost --portDestination $adminPort2 -n -X"
    else
      echo "$DSREPLICATION initialize --baseDN \"$baseDN\" --adminUID admin --adminPassword XXXXXXXXXX --hostSource $initHost --portSource $adminPort --hostDestination $newHost --portDestination $adminPort2 -n -X"
    fi
    echo "Preparing to execute Step $COUNT"
    read -p "Execute? [y/s/q]: " ANS
    if [[ "$ANS" == "y" ]]; then
      $DSREPLICATION initialize --baseDN "$baseDN" --adminUID admin --adminPassword "$adminPass" --hostSource $initHost --portSource $adminPort --hostDestination $newHost --portDestination $adminPort2 -n -X
    elif [[ "$ANS" == "s" ]]; then
      echo "Skipping command"
      echo
    else
      echo "Quitting at step $COUNT."
      exit 1
    fi
    echo
    COUNT=$((COUNT+1))
  done

  if [ "$groupId" -ne 1 ]; then
    echo "# Step $COUNT: Set the Replication Server Group ID on $initHost to $groupId"
    if [ $SHOWPASS -eq 1 ]; then
      echo "$DSCONFIG set-replication-server-prop --provider-name \"Multimaster Synchronization\" --set group-id:$groupId --hostname $initHost --port $adminPort --bindDN \"cn=Directory Manager\" --bindPassword '$bindPass' -n -X"
    else
      echo "$DSCONFIG set-replication-server-prop --provider-name \"Multimaster Synchronization\" --set group-id:$groupId --hostname $initHost --port $adminPort --bindDN \"cn=Directory Manager\" --bindPassword XXXXXXXXXX -n -X"
    fi
    echo "Preparing to execute Step $COUNT"
    read -p "Execute? [y/s/q]: " ANS
    if [[ "$ANS" == "y" ]]; then
      $DSCONFIG set-replication-server-prop --provider-name "Multimaster Synchronization" --set group-id:$groupId --hostname $initHost --port $adminPort --bindDN "cn=Directory Manager" --bindPassword "$bindPass" -n -X
    elif [[ "$ANS" == "s" ]]; then
      echo "Skipping command"
      echo
    else
      echo "Quitting at step $COUNT."
      exit 1
    fi
    echo
    COUNT=$((COUNT+1))
  fi

  if [ $(contains "${noReplicationServerHosts[@]}" "$newHost") == "n" ];
  then
    if [ "$groupId" -ne 1 ]; then
      echo "# Step $COUNT: Set the Replication Server Group ID on $newHost to $groupId"
      if [ $SHOWPASS -eq 1 ]; then
        echo "$DSCONFIG set-replication-server-prop --provider-name \"Multimaster Synchronization\" --set group-id:$groupId --hostname $newHost --port $adminPort2 --bindDN \"cn=Directory Manager\" --bindPassword '$bindPass' -n -X"
      else
        echo "$DSCONFIG set-replication-server-prop --provider-name \"Multimaster Synchronization\" --set group-id:$groupId --hostname $newHost --port $adminPort2 --bindDN \"cn=Directory Manager\" --bindPassword XXXXXXXXXX -n -X"
       fi
      echo "Preparing to execute Step $COUNT"
      read -p "Execute? [y/s/q]: " ANS
      if [[ "$ANS" == "y" ]]; then
        $DSCONFIG set-replication-server-prop --provider-name "Multimaster Synchronization" --set group-id:$groupId --hostname $newHost --port $adminPort2 --bindDN "cn=Directory Manager" --bindPassword "$bindPass" -n -X
      elif [[ "$ANS" == "s" ]]; then
        echo "Skipping command"
        echo
      else
        echo "Quitting at step $COUNT."
        exit 1
      fi
      echo
      COUNT=$((COUNT+1))
    fi

    if [[ "$purgeDelay" != "3d" && ! -z $purgeDelay ]]; then
      if [ "$initPurgeDelay" -eq 0 ]; then
        echo ""
        echo "# Step $COUNT: Set then Change Log Purge Delay on $initHost to $purgeDelay"
        if [ $SHOWPASS -eq 1 ]; then
          echo "$DSCONFIG set-replication-server-prop --provider-name \"Multimaster Synchronization\" --advanced --set replication-purge-delay:$purgeDelay --hostname $initHost --port $adminPort --bin dDN \"cn=Directory Manager\" --bindPassword '$bindPass' -n -X"
        else
          echo "$DSCONFIG set-replication-server-prop --provider-name \"Multimaster Synchronization\" --advanced --set replication-purge-delay:$purgeDelay --hostname $initHost --port $adminPort --bin dDN \"cn=Directory Manager\" --bindPassword XXXXXXXXXX -n -X"
        fi
        echo "Preparing to execute Step $COUNT"
        read -p "Execute? [y/s/q]: " ANS
        if [[ "$ANS" == "y" ]]; then
          $DSCONFIG set-replication-server-prop --provider-name "Multimaster Synchronization" --advanced --set replication-purge-delay:$purgeDelay --hostname $initHost --port $adminPort --bindDN "cn=Directory Manager" --bindPassword "$bindPass" -n -X
        elif [[ "$ANS" == "s" ]]; then
          echo "Skipping command"
          echo
        else
          echo "Quitting at step $COUNT."
          exit 1
        fi
        COUNT=$((COUNT+1))
        initPurgeDelay=1
      fi

      echo ""
      echo "# Step $COUNT: Set then Change Log Purge Delay on $newHost to $purgeDelay"
      if [ $SHOWPASS -eq 1 ]; then
        echo "$DSCONFIG set-replication-server-prop --provider-name \"Multimaster Synchronization\" --advanced --set replication-purge-delay:$purgeDelay --hostname $newHost --port $adminPort2 --bin dDN \"cn=Directory Manager\" --bindPassword '$bindPass' -n -X"
      else
        echo "$DSCONFIG set-replication-server-prop --provider-name \"Multimaster Synchronization\" --advanced --set replication-purge-delay:$purgeDelay --hostname $newHost --port $adminPort2 --bin dDN \"cn=Directory Manager\" --bindPassword XXXXXXXXXX -n -X"
      fi
      echo "Preparing to execute Step $COUNT"
      read -p "Execute? [y/s/q]: " ANS
      if [[ "$ANS" == "y" ]]; then
        $DSCONFIG set-replication-server-prop --provider-name "Multimaster Synchronization" --advanced --set replication-purge-delay:$purgeDelay --hostname $newHost --port $adminPort2 --bindDN "cn=Directory Manager" --bindPassword "$bindPass" -n -X
      elif [[ "$ANS" == "s" ]]; then
        echo "Skipping command"
        echo
      else
        echo "Quitting at step $COUNT."
        exit 1
      fi
      echo
      COUNT=$((COUNT+1))
    fi

  else
    echo "# Step $COUNT: Disable the Replication Server"
    if [ $SHOWPASS -eq 1 ]; then
      echo "$DSREPLICATION disable --disableReplicationServer --adminUID admin --adminPassword \"$adminPass\" -n -X"
    else
      echo "$DSREPLICATION disable --disableReplicationServer --adminUID admin --adminPassword XXXXXXXXXX -n -X"
    fi
    echo "Preparing to execute Step $COUNT"
    read -p "Execute? [y/s/q]: " ANS
    if [[ "$ANS" == "y" ]]; then
      $DSREPLICATION disable --disableReplicationServer --adminUID admin --adminPassword "$adminPass" -n -X
    elif [[ "$ANS" == "s" ]]; then
      echo "Skipping command"
      echo
    else
      echo "Quitting at step $COUNT."
      exit 1
    fi
    echo
    COUNT=$((COUNT+1))
  fi

  if [ "$groupId" -ne 1 ]; then
    if [ "$initSchemaGroupId" -eq 0 ]; then
      echo "# Step $COUNT: Set the Group ID of cn=schema on $initHost to $groupId"
      if [ $SHOWPASS -eq 1 ]; then
        echo "$DSCONFIG set-replication-domain-prop --provider-name \"Multimaster Synchronization\" --domain-name \"cn=schema\" --set group-id:$groupId --hostname $initHost --port $adminPort --bindDN \"cn=Directory Manager\" --bindPassword '$bindPass' -n -X"
      else
        echo "$DSCONFIG set-replication-domain-prop --provider-name \"Multimaster Synchronization\" --domain-name \"cn=schema\" --set group-id:$groupId --hostname $initHost --port $adminPort --bindDN \"cn=Directory Manager\" --bindPassword XXXXXXXXXX -n -X"
      fi
      echo "Preparing to execute Step $COUNT"
      read -p "Execute? [y/s/q]: " ANS
      if [[ "$ANS" == "y" ]]; then
        $DSCONFIG set-replication-domain-prop --provider-name "Multimaster Synchronization" --domain-name "cn=schema" --set group-id:$groupId --hostname $initHost --port $adminPort --bindDN "cn=Directory Manager" --bindPassword "$bindPass" -n -X
        initSchemaGroupId=1
      elif [[ "$ANS" == "s" ]]; then
        echo "Skipping command"
        echo
      else
        echo "Quitting at step $COUNT."
        exit 1
      fi
      echo
      COUNT=$((COUNT+1))
    fi

    echo "# Step $COUNT: Set the Group ID of cn=schema on $newHost to $groupId"
    if [ $SHOWPASS -eq 1 ]; then
      echo "$DSCONFIG set-replication-domain-prop --provider-name \"Multimaster Synchronization\" --domain-name \"cn=schema\" --set group-id:$groupId --hostname $newHost --port $adminPort2 --bindDN \"cn=Directory Manager\" --bindPassword '$bindPass' -n -X"
    else
      echo "$DSCONFIG set-replication-domain-prop --provider-name \"Multimaster Synchronization\" --domain-name \"cn=schema\" --set group-id:$groupId --hostname $newHost --port $adminPort2 --bindDN \"cn=Directory Manager\" --bindPassword XXXXXXXXXX -n -X"
    fi
    echo "Preparing to execute Step $COUNT"
    read -p "Execute? [y/s/q]: " ANS
    if [[ "$ANS" == "y" ]]; then
      $DSCONFIG set-replication-domain-prop --provider-name "Multimaster Synchronization" --domain-name "cn=schema" --set group-id:$groupId --hostname $newHost --port $adminPort2 --bindDN "cn=Directory Manager" --bindPassword "$bindPass" -n -X
    elif [[ "$ANS" == "s" ]]; then
      echo "Skipping command"
      echo
    else
      echo "Quitting at step $COUNT."
      exit 1
    fi
    echo
    COUNT=$((COUNT+1))

    if [ "$initAdminGroupId" -eq 0 ]; then
      echo "# Step $COUNT: Set the Group ID of cn=admin data on $initHost to $groupId"
      if [ $SHOWPASS -eq 1 ]; then
        echo "$DSCONFIG set-replication-domain-prop --provider-name \"Multimaster Synchronization\" --domain-name \"cn=admin data\" --set group-id:$groupId --hostname $initHost --port $adminPort --bindDN \"cn=Directory Manager\" --bindPassword '$bindPass' -n -X"
      else
        echo "$DSCONFIG set-replication-domain-prop --provider-name \"Multimaster Synchronization\" --domain-name \"cn=admin data\" --set group-id:$groupId --hostname $initHost --port $adminPort --bindDN \"cn=Directory Manager\" --bindPassword XXXXXXXXXX -n -X"
      fi
      echo "Preparing to execute Step $COUNT"
      read -p "Execute? [y/s/q]: " ANS
      if [[ "$ANS" == "y" ]]; then
        $DSCONFIG set-replication-domain-prop --provider-name "Multimaster Synchronization" --domain-name "cn=admin data" --set group-id:$groupId --hostname $initHost --port $adminPort --bindDN "cn=Directory Manager" --bindPassword "$bindPass" -n -X
        initAdminGroupId=1
      elif [[ "$ANS" == "s" ]]; then
        echo "Skipping command"
        echo
      else
        echo "Quitting at step $COUNT."
        exit 1
      fi
      echo
      COUNT=$((COUNT+1))
    fi

    echo "# Step $COUNT: Set the Group ID of cn=admin data on $newHost to $groupId"
    if [ $SHOWPASS -eq 1 ]; then
      echo "$DSCONFIG set-replication-domain-prop --provider-name \"Multimaster Synchronization\" --domain-name \"cn=admin data\" --set group-id:$groupId --hostname $newHost --port $adminPort2 --bindDN \"cn=Directory Manager\" --bindPassword '$bindPass' -n -X"
    else
      echo "$DSCONFIG set-replication-domain-prop --provider-name \"Multimaster Synchronization\" --domain-name \"cn=admin data\" --set group-id:$groupId --hostname $newHost --port $adminPort2 --bindDN \"cn=Directory Manager\" --bindPassword XXXXXXXXXX -n -X"
    fi
    echo "Preparing to execute Step $COUNT"
    read -p "Execute? [y/s/q]: " ANS
    if [[ "$ANS" == "y" ]]; then
      $DSCONFIG set-replication-domain-prop --provider-name "Multimaster Synchronization" --domain-name "cn=admin data" --set group-id:$groupId --hostname $newHost --port $adminPort2 --bindDN "cn=Directory Manager" --bindPassword "$bindPass" -n -X
    elif [[ "$ANS" == "s" ]]; then
      echo "Skipping command"
      echo
    else
      echo "Quitting at step $COUNT."
      exit 1
    fi
    echo
    COUNT=$((COUNT+1))
  fi
done


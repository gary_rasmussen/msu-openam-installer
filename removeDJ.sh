#!/bin/bash

# Prevents this script from running if any of the variables are not defined
set -u

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ "$#" -eq '1' ]; then
  if [ ! -r "$SCRIPTDIR/artifacts/config/OpenDJ/$1" ]; then
    echo ""
    echo "ERROR: File $SCRIPTDIR/artifacts/config/OpenDJ/$1"
    echo "isn't readable or doesn't exist. Please correct the script argument or"
    echo "restore the properties file and attempt the installation again."
    exit 1
  else
    # read environment specific parameters from properties file
    source $SCRIPTDIR/artifacts/config/OpenDJ/$1
  fi
elif [ "$#" -gt '1' ]; then
  echo ""
  echo "ERROR: Too many arguments were provided to this script. Please either"
  echo "specify one properties file name or use no arguments to use the default."
  exit 2
else
  echo ""
  echo "No arguments provided. Defaulting to the properties file opendj.properties"
  if [ ! -r "$SCRIPTDIR/artifacts/config/OpenDJ/opendj.properties" ]; then
    echo ""
    echo "ERROR: File $SCRIPTDIR/artifacts/config/OpenDJ/opendj.properties"
    echo "isn't readable or doesn't exist. Please correct the script argument or"
    echo "restore the properties file and attempt the installation again."
    exit 3
  else
    # read environment specific parameters from properties file
    source $SCRIPTDIR/artifacts/config/OpenDJ/$1
  fi
fi

echo
echo "This script will stop the following OpenDJ process: $ODJFQDN:$LDAPPORT"
echo "and REMOVE ALL CONTENTS from the following folders:  $DJDEST"
echo "$DJSRC"
echo
read -p "Are you ABSOLUTELY SURE that you want to continue? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo
    if [ -d "$DJDEST" ]; then

        echo "OpenDJ was found in:  $DJDEST"
        echo
        echo "Stopping OpenDJ instance..."
        $DJDEST/bin/stop-ds
        echo
        echo "Removing OpenDJ Folder..."
        rm -rf $DJDEST

    else

        echo "OpenDJ is not installed in:  $DJDEST"
        echo "Not action taken"

    fi

    if [ -d "$DJSRC" ]; then

        echo "Removing OpenDJ Temporary files..."
        rm -rf $DJSRC

    fi
fi

echo


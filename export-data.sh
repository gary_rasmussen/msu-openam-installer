#!/bin/bash
shopt -s nocasematch

#set -x

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


if [ "$#" -eq '1' ]; then
  if [ ! -r "$SCRIPTDIR/artifacts/config/$1" ]; then
    echo ""
    echo "ERROR: File $SCRIPTDIR/artifacts/config/$1"
    echo "isn't readable or doesn't exist. Please correct the script argument or"
    echo "restore the properties file and attempt the installation again."
    exit 1
  else
    # read environment specific parameters from properties file
    source $SCRIPTDIR/artifacts/config/$1
  fi
elif [ "$#" -gt '1' ]; then
  echo ""
  echo "ERROR: Too many arguments were provided to this script. Please either"
  echo "specify one properties file name or use no arguments to use the default."
  exit 2
else
  echo ""
  echo "No arguments provided. Defaulting to the properties file opendj.properties"
  if [ ! -r "$SCRIPTDIR/artifacts/config/opendj.properties" ]; then
    echo ""
    echo "ERROR: File $SCRIPTDIR/artifacts/config/opendj.properties"
    echo "isn't readable or doesn't exist. Please correct the script argument or"
    echo "restore the properties file and attempt the installation again."
    exit 3
  else
    # read environment specific parameters from properties file
    source $SCRIPTDIR/artifacts/config/$1
  fi
fi

if [ ! -d $DJDEST ]; then
  echo ""
  echo "ERROR! No DJ instance installed at $DJDEST."
  echo "Please ensure variable DJDEST specifies the correct location of the DJ instance"
  exit 1
fi


if [[ ! -z "$DJPRESERVE" && ! -e "$DJPRESERVE" ]]; then
  mkdir $DJPRESERVE
fi

if [[ ! -e "$DJPRESERVE/$INSTANCEDIRNAME" ]]; then
  mkdir $DJPRESERVE/$INSTANCEDIRNAME
fi

if [[ -e $DJDEST/config/java.properties ]]; then
  cp $DJDEST/config/java.properties $DJPRESERVE/$INSTANCEDIRNAME/java.properties
fi

for i in "${BACKENDS[@]}"
do
    BASE=`echo $i | cut -f1 -d^`
    DBNAME=`echo $i | cut -f2 -d^`

    echo ""
    echo "##############################################################################"
    echo "DJ: Exporting entries in $BASE within backend $DBNAME..."
    echo "##############################################################################"

    $DJDEST/bin/export-ldif -l $DJPRESERVE/$INSTANCEDIRNAME/$DBNAME-$BASE.ldif -n $DBNAME -b $BASE
done

exit

# THIS LOGIC IS USED IF DATA MUST BE SPLIT WITHIN AN EXISTING OPENDJ INSTANCE
# UPDATE WITH THE GUIDANCE OF THE DIRECTORY SERVER SME

for i in "${BACKENDS[@]}"
do
    BASE=`echo $i | cut -f1 -d^`
    DBNAME=`echo $i | cut -f2 -d^`

    if [[ "$BASE" == "dc=example,dc=com" ]]; then
      echo ""
      echo "##############################################################################"
      echo "DJ: Exporting entries in $BASE within backend $DBNAME..."
      echo "##############################################################################"

      $DJDEST/bin/export-ldif -l $DJPRESERVE/opendj-user/$DBNAME-$BASE.ldif -n $DBNAME -b $BASE
    elif [[ "$BASE" == "dc=example,dc=net"  ]]; then
      echo ""
      echo "##############################################################################"
      echo "DJ: Exporting Config entries (excluding Tokens) in $BASE within backend $DBNAME..."
      echo "##############################################################################"

      $DJDEST/bin/export-ldif -l $DJPRESERVE/opendj-config/$DBNAME-$BASE.ldif -n $DBNAME -b $BASE -B "$BASE"


      echo ""
      echo "##############################################################################"
      echo "DJ: Exporting Tokens (excluding all else) in $BASE within backend $DBNAME..."
      echo "##############################################################################"

      $DJDEST/bin/export-ldif -l $DJPRESERVE/opendj-cts/$DBNAME-ou=tokens,$BASE.ldif -n $DBNAME -b "ou=tokens,$BASE"

    fi
done
